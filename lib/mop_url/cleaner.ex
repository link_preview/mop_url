defmodule MopURL.Cleaner do
  @moduledoc """
  Cleans URLs using rules.

  Load rules using `new/0` or `new/1`.

  Rules file is a json file of the form:
  ```json
  {
      "providers": {
          "rule-label: {
              // ... rule
          }
      }
  }
  ```

  See `MopURL.Rule` for explanation about rule format.
  """

  alias MopURL.Rule

  def new do
    Application.app_dir(:mop_url, "priv/clearurls_rules/data.min.json")
    |> new()
  end

  def new(path) do
    path
    |> File.read!()
    |> Jason.decode!()
    |> Map.get("providers")
    |> Enum.map(fn {k, v} -> {k, Rule.new(v)} end)
    |> Enum.into(%{})
  end

  # defp any_match?(list, string) do
  #  Enum.any?(list, &Regex.match?(&1, string))
  # end

  # def match_condition?(rule, url) do
  #  Regex.match?(rule.urlPattern, url) and
  #    not any_match?(rule.exceptions, url)
  # end

  def clean(cleaner, url) do
    new_url =
      Enum.find_value(cleaner, url, fn {_rule_label, rule} ->
        if Regex.match?(rule.urlPattern, url) do
          new_url = Rule.apply(rule, url)

          if new_url == url do
            false
          else
            new_url
          end
        else
          false
        end
      end)

    if new_url == url do
      new_url
    else
      # Double wipe to make sure it's clean!
      clean(cleaner, new_url)
    end
  end
end
