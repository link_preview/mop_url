defmodule MopURL.ValidateURL do
  @moduledoc """
  URL validator

  ## Usage:

      iex> good_url = cast("google.com")
      {:ok, "https://google.com"}
      iex> validate_hostname(good_url)
      :ok

      iex> bad_url = cast("localhost")
      {:ok, "https://localhost"}
      iex> validate_hostname(bad_url)
      {:error, "restricted ip"}

      iex> cast("cant-reach-this.com") |> validate_hostname()
      {:error, :nxdomain}

  """

  @link_regex Regex.compile!(
                # protocol
                # user:pass
                # IP
                # host/domain
                # TLD
                # may end with a dot
                # port
                # path
                "^" <>
                  "(?:(?:https?|ftp)://)" <>
                  "(?:\\S+(?::\\S*)?@)?" <>
                  "(?:" <>
                  "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" <>
                  "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" <>
                  "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" <>
                  "|" <>
                  "(?:(?:[a-z\\x{00a1}-\\x{ffff}0-9]-*)*[a-z\\x{00a1}-\\x{ffff}0-9]+)" <>
                  "(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}0-9]-*)*[a-z\\x{00a1}-\\x{ffff}0-9]+)*" <>
                  "\\.?" <>
                  ")" <>
                  "(?::\\d{2,5})?" <>
                  "(?:[/?#]\\S*)?" <>
                  "$",
                "iu"
              )

  @doc """
  Regex that matches properly formatted URLs.
  Adapted from https://github.com/spence/valid_url
  and https://gist.github.com/dperini/729294
  """
  def link_regex, do: @link_regex

  @doc """
  Tries to convert given string to a valid URL.

  ## Examples

      iex> cast("HtTps://GOogLE.cOM/search?q=2+323+32")
      {:ok, "https://google.com/search?q=2+323+32"}

      iex> cast("localhost")                           
      {:ok, "https://localhost"}

      iex> cast("instagram.com")
      {:ok, "https://instagram.com"}

  """
  def cast(url) do
    url =
      if is_valid_format(url) do
        url
      else
        try_fix_format(url)
      end
      |> lowercase_baseurl()

    do_validate_format(url)
  end

  defp do_validate_format(url) do
    if is_valid_format(url) do
      {:ok, url}
    else
      {:error, "format"}
    end
  end

  defp lowercase_baseurl(url) do
    uri = URI.parse(url)

    new_url =
      %{uri | authority: nil_downcase(uri.authority), host: nil_downcase(uri.host)}
      |> URI.to_string()

    new_url
  end

  defp nil_downcase(nil), do: nil
  defp nil_downcase(str), do: String.downcase(str)

  @doc """
  Checks url format using a regular expression.

  ## Examples

      iex> is_valid_format("HtTps://GOogLE.cOM/search?q=elixir+fresh")
      true

      iex> is_valid_format("ipfs://GOogLE.cOM/search?q=elixir+fresh")
      false

  """
  def is_valid_format(url) do
    Regex.match?(link_regex(), url)
  end

  defp try_fix_format(url) do
    "https://#{url}"
  end

  @doc """
  Checks that given URL has reachable hostname

  Also makes sure that ip of this hostname is not restricted
  (doesn't point to local network)

  ## Examples

      iex> validate_hostname("https://localhost")                           
      {:error, "restricted ip"}

      iex> validate_hostname("https://google.com")
      :ok

  """
  def validate_hostname(url) when is_binary(url) do
    validate_hostname({:ok, url})
  end

  def validate_hostname({:ok, url}) when is_binary(url) do
    url
    |> do_resolve_hostname()
    |> do_check_restricted_ip()
  end

  defp do_resolve_hostname(url) do
    host = URI.parse(url).host

    case resolve_hostname(host) do
      {:ok, ips} -> {:ok, ips}
      {:error, error} -> {:error, error}
    end
  end

  defp do_check_restricted_ip({:ok, ips}) do
    if Enum.any?(ips, &is_restricted_ip/1) do
      {:error, "restricted ip"}
    else
      :ok
    end
  end

  defp do_check_restricted_ip({:error, error}) do
    {:error, error}
  end

  @doc """
  Checks if hostname is resolvable.
  """
  def resolve_hostname(hostname) when is_binary(hostname) do
    charname = Kernel.to_charlist(hostname)

    case :inet.gethostbyname(charname) do
      {:ok, {_, _, _, _, _, ips}} ->
        {:ok, Enum.map(ips, &IP.Address.from_tuple!/1)}

      {:error, error} ->
        {:error, error}
    end
  end

  @doc """
  Checks if a `ip` is restricted (not in global scope)

  ## Examples

      iex> IP.Address.from_string!("127.0.0.1") |> is_restricted_ip()
      true

      iex> IP.Address.from_string!("0.0.0.0") |> is_restricted_ip()
      true
      
      iex> IP.Address.from_string!("142.251.215.238") |> is_restricted_ip()
      false
  """
  def is_restricted_ip(%IP.Address{} = ip) do
    IP.Scope.address_scope(ip) != "GLOBAL UNICAST"
  end
end
