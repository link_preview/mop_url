defmodule MopURL.MixProject do
  use Mix.Project

  @name "MopURL"
  @version "0.1.0"
  @repo_url "https://gitlab.com/link_preview/mop_url"

  def project do
    [
      app: :mop_url,
      version: @version,
      elixir: "~> 1.13",
      description: description(),
      package: package(),
      docs: docs(),
      start_permanent: Mix.env() == :prod,
      name: @name,
      source_url: @repo_url,
      deps: deps()
    ]
  end

  defp description() do
    "Casts strings to urls, validates hostnames, and removes redundant and tracking elements."
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.3"},
      {:ip, "~> 1.2"},
      {:credo, "~> 1.6", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.1", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.28.4", only: :dev, runtime: false}
    ]
  end

  def package do
    [
      licenses: ["MIT"],
      links: %{"GitLab" => @repo_url}
    ]
  end

  def docs do
    [
      logo: "assets/mop_negated.png",
      extras: ["README.md"],
      assets: "assets",
      source_ref: "v#{@version}",
      source_url: @repo_url,
      main: @name
    ]
  end
end
