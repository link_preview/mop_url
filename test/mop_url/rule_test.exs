defmodule MopURL.RuleTest do
  use ExUnit.Case
  alias MopURL.{Cleaner, Rule}
  doctest MopURL.Rule

  test "simplest rule" do
    rule = Rule.new(%{"urlPattern" => ".*", "completeProvider" => false})
    assert rule.urlPattern == ~r/.*/
    assert rule.completeProvider == false
  end

  test "clean ebay" do
    url =
      "https://www.ebay.com/itm/115435902915?_trkparms=amclksrc%3DITM%26aid%3D777008%26algo%3DPERSONAL.TOPIC%26ao%3D1%26asc%3D20200708143445%26meid%3D71d7356188f7455d8ee9ba20b514dc02%26pid%3D101251%26rk%3D1%26rkt%3D1%26itm%3D115435902915%26pmt%3D0%26noa%3D1%26pg%3D2380057%26algv%3DPersonalizedTopicsV2WithMetaOrganicPRecall%26brand%3DCallaway&_trksid=p2380057.c101251.m47269&_trkparms=pageci%3Aa9d9caab-f7d5-11ec-acbe-46c1b932396c%7Cparentrq%3Ab0a1224a1810a120d4af3f25fff80488%7Ciid%3A1"

    cleaner = Cleaner.new()
    rule = cleaner["ebay"]
    clean_url = Rule.apply(rule, url)

    assert clean_url != url
    assert clean_url == "https://www.ebay.com/itm/115435902915"
  end

  test "redirect reddit" do
    url =
      "https://out.reddit.com/t3_559sfb?url=http%3A%2F%2Faldusleaf.org%2Fmonitoring-elixir-apps-in-2016-prometheus-and-grafana%2F&token=AQAAaCSWYi6p4GlJn6YyqTbsTznejJJGr5vZLq-k83BPdZQ3mIwE&app_name=web2x&web_redirect=true"

    cleaner = Cleaner.new()
    rule = cleaner["reddit"]
    clean_url = Rule.apply(rule, url)

    assert clean_url != url

    assert clean_url ==
             "http://aldusleaf.org/monitoring-elixir-apps-in-2016-prometheus-and-grafana/"
  end
end
