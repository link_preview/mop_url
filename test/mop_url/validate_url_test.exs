defmodule MopURL.ValidateURLTest do
  use ExUnit.Case
  alias MopURL.ValidateURL
  doctest MopURL.ValidateURL, import: true

  # Tests taken from https://mathiasbynens.be/demo/url-regex

  test "valid URL formats" do
    urls = [
      "http://foo.com/blah_blah",
      "http://foo.com/blah_blah/",
      "http://foo.com/blah_blah_(wikipedia)",
      "http://foo.com/blah_blah_(wikipedia)_(again)",
      "http://www.example.com/wpstyle/?p=364",
      "https://www.example.com/foo/?bar=baz&inga=42&quux",
      "http://✪df.ws/123",
      "http://userid:password@example.com:8080",
      "http://userid:password@example.com:8080/",
      "http://userid@example.com",
      "http://userid@example.com/",
      "http://userid@example.com:8080",
      "http://userid@example.com:8080/",
      "http://userid:password@example.com",
      "http://userid:password@example.com/",
      "http://142.42.1.1/",
      "http://142.42.1.1:8080/",
      "http://➡.ws/䨹",
      "http://⌘.ws",
      "http://⌘.ws/",
      "http://foo.com/blah_(wikipedia)#cite-1",
      "http://foo.com/blah_(wikipedia)_blah#cite-1",
      "http://foo.com/unicode_(✪)_in_parens",
      "http://foo.com/(something)?after=parens",
      "http://☺.damowmow.com/",
      "http://code.google.com/events/#&product=browser",
      "http://j.mp",
      "ftp://foo.bar/baz",
      "http://foo.bar/?q=Test%20URL-encoded%20stuff",
      "http://مثال.إختبار",
      "http://例子.测试",
      "http://उदाहरण.परीक्षा",
      "http://-.~_!$&'()*+,;=:%40:80%2f::::::@example.com",
      "http://1337.net",
      "http://a.b-c.de",
      "http://a.b--c.de/",
      "http://www.foo.bar./",
      "http://223.255.255.254",
      "http://www.example.com.",
      "http://www.example.com/",
      "http://www.example.com?",
      "http://www.example.com#",
      "http://www.example.com./?#",
      "http://0.0.0.0",
      "http://10.1.1.0",
      "http://10.1.1.255",
      "http://224.1.1.1",
      "http://1.1.1.1.1",
      "http://123.123.123",
      "http://3628126748",
      "http://10.1.1.1"
    ]

    for url <- urls do
      assert {:ok, url} == ValidateURL.cast(url),
             "Expected \"#{url}\" to be a valid URL."
    end
  end

  test "invalid URL formats" do
    invalid_urls = [
      "http://",
      "http://.",
      "http://..",
      "http://../",
      "http://?",
      "http://??",
      "http://??/",
      "http://#",
      "http://##",
      "http://##/",
      "http://foo.bar?q=Spaces should be encoded",
      "//",
      "//a",
      "///a",
      "///",
      "http:///a",
      "foo.com",
      "rdar://1234",
      "h://test",
      "http:// shouldfail.com",
      ":// should fail",
      "http://foo.bar/foo(bar)baz quux",
      "ftps://foo.bar/",
      "http://-error-.invalid/",
      "http://-a.b.co",
      "http://a.b-.co",
      "http://.www.foo.bar/",
      "http://.www.foo.bar./"
    ]

    for invalid_url <- invalid_urls do
      assert {:ok, invalid_url} != ValidateURL.cast(invalid_url),
             "Expected \"#{invalid_url}\" to be an invalid URL."
    end
  end
end
