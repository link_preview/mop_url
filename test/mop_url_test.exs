defmodule MopURLTest do
  use ExUnit.Case
  doctest MopURL

  test "redirect test" do
    url =
      "https://www.google.com/search?q=elixir&source=hp&ei=safsa&oq=elixir&gs_lcp=gfsdsadh"
      |> URI.encode_www_form()

    url1 =
      "https://out.reddit.com/t3_559sfb?url=#{url}&token=AQAAaCSWYi6p4GlJn6YyqTbsTznejJJGr5vZLq-k83BPdZQ3mIwE&app_name=web2x&web_redirect=true"

    MopURL.start_link(name: MyMopURL)
    result = MopURL.clean(MyMopURL, url1)

    assert {:ok, "https://www.google.com/search?q=elixir"} == result
  end
end
